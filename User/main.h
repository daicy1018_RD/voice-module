#ifndef MAIN_H
#define	MAIN_H

#include <stdbool.h>
#include <stdint.h>

#define BIN_START_ADDR					0x10000		// 64KB

#define MEM_SIZE						7972//8192
#define MAX_TIME						300			//3s
#define STATE_SIZE						64

#define MIC_BUFF_SIZE					160

/* System Management socket */
typedef struct {
    uint16_t pm25_rx_update:		1;
	
} __sys_bits_t;

typedef struct Sys_Management {
	__sys_bits_t status;
	uint8_t	 mode;
	uint8_t  reserved;
	uint16_t pm2_5;
	uint16_t pm25_raw;
	uint16_t pm25_Vref;
}sysmgmt_t;

extern sysmgmt_t SysMgmt;

extern __align(4) uint8_t lpMemPool[MEM_SIZE];
extern __align(4) uint8_t lpState[STATE_SIZE];

void PDMA0_Callback( void );

#endif	/* MAIN_H */

