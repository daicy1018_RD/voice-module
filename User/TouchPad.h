#ifndef TOUCH_PAD_H
#define	TOUCH_PAD_H

#include <stdbool.h>
#include <stdint.h>

#define TP_KEY_NBR       		(1)
#define TP_KEY_NBR_MAX			(8)
#define TP_CALIB_AVG   			(16)
#define TP_LOW_STEP				(30)
#define TP_GRAND_CNTS        	(2)

typedef struct t_Key {
	uint16_t pre_Cnt;
	uint16_t rel_Cnt;
	uint16_t grand;
	uint16_t reserved;
	int32_t  lower;
	int32_t  current;
}tKey_t;

typedef struct Touch_Pad {
	uint16_t status;
	uint16_t enPin;
	uint16_t index;
	tKey_t   tKey[TP_KEY_NBR];
}TouchPad_t;

extern TouchPad_t TouchPad;

void     TouchPad_Scan( void );
void     TouchPad_Init( uint16_t u16Pin );
uint16_t TouchPad_GetStatus( void );
void 	 TouchPad_ClrStatus( void );

#endif	/* TOUCH_PAD_H */

