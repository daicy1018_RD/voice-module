#ifndef SYS_INIT_H
#define	SYS_INIT_H

#include <stdbool.h>
#include <stdint.h>

typedef enum {
	PDMA_CH_0 = 0,
    PDMA_CH_1,
	PDMA_CH_2,
	PDMA_CH_3,
} pdma_channel_t;

typedef enum {
	PDMA_RX_APB	= 0,
	PDMA_TX_APB = 1
} pdma_tx_rx_t;

void sys_Init( void );
void Clk_Init( void );
void LDO_On( void );
void SPI_Init( void );
void ANA_Init( void );
void ADC_Init( void );
void PDMA_Init( void );
void Uart_Init( void );
void tPad_Init( void );
void DPWM_Init( uint32_t SampleRate );
void PDMA_Initfor( uint8_t chnnl,  	\
				   uint32_t dst, 	\
				   uint32_t src, 	\
				   uint32_t dir, 	\
				   uint32_t ch4tr, 	\
				   uint32_t buflen );
void PDMA_TxEn( uint8_t chnnl, bool enable );

#endif	/* SYS_INIT_H */

