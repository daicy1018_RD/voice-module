/*******************************************************************************

   (C) Copyright 2016/7/15, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: Tmr.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/15
     Discription:

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version:
          Author: Chaoyi Dai
Last Modify Date: 2016/7/15
     Discription: Initial version

*******************************************************************************/

#include "includes.h"
#include "gpio.h"

TimerFlag_t TimerFlag;
static uint32_t tick = 0;
void SysTimerHandler( void )
{
    TimerFlag_t* timer = &TimerFlag;
	
    tick++;
	timer->trig_10ms = true;
	
	if( (tick % TICK_20_ms) == 0 ) {            // 20ms
		timer->trig_20ms = true;
    }
	
    if( (tick % TICK_50_ms) == 0 ) {            // 50ms
		timer->trig_50ms = true;
    }
	
	if( (tick % TICK_100_ms) == 0 ) {           // 100ms
		timer->trig_100ms = true;
    }
	
    if( (tick % TICK_500_ms) == 0 ) {           // 500ms
		timer->trig_500ms = true;
    }
	
	if( (tick % TICK_1000_ms) == 0 ) {          // 1000ms
		tick = 0;
		timer->trig_1s = true;
	}
}

void SysTimerReset( void )
{
	TimerFlag_t* timer = &TimerFlag;
	
	tick = 0;
	timer->trig_10ms = false;
	timer->trig_20ms = false;
	timer->trig_50ms = false;
	timer->trig_100ms = false;
	timer->trig_500ms = false;
	timer->trig_1s = false;
}
