/*******************************************************************************

   (C) Copyright 2016/7/20, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: TouchPad.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/20
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: 
          Author: Chaoyi Dai
Last Modify Date: 2016/7/20
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


const uint16_t THRESHOLD_PRESS[TP_KEY_NBR] = {800/*, 1200, 750, 800, 800, 750, 800, 1200*/};
const uint16_t THRESHOLD_RELEASE[TP_KEY_NBR] = {350/*, 200, 300, 350, 350, 300, 350, 200*/};
TouchPad_t TouchPad;
volatile uint8_t u8CapSenseIRQFlag;

/**
  * @brief  This function is capture sense's interrupt handler. 
  * @param  None.
  * @return None
  */
void CAPS_IRQHandler( void )
{
    u8CapSenseIRQFlag = 1;
	CAPSENSE_DISABLE_INTERRUPT();
}

/**
  * @brief  This function is to reset capture sense's counter & interrupt control(internal function). 
  * @param  None.
  * @return None
  */
void TouchPad_Reset( void )
{
	CapSense_ResetCounter();
	CAPSENSE_ENABLE_INTERRUPT();
	CAPSENSE_ENABLE();
}

/**
  * @brief  This function is to calibrate touch pad initiate value(internal function). 
  * @param  None.
  * @return None
  */
void TouchPad_Calib( void )
{
	uint8_t i;
	uint8_t j;
	uint8_t ki = 0;
	uint32_t count;
	TouchPad_t* tpk = &TouchPad;
	
	for( i=0; i<TP_KEY_NBR_MAX; i++ ) {
		if( tpk->enPin & (0x1 << i) ) {
			ACMP->POSSEL = i << ACMP_POSSEL_POSSEL_Pos;
			ACMP->CTL0  &= (~ACMP_CTL0_NEGSEL_Msk);
			ACMP->CTL0  |= ACMP_CTL0_ACMPEN_Msk;
			count = 0;
			
			for( j=0; j<TP_CALIB_AVG; j++ ) {
				TouchPad_Reset();
				while( u8CapSenseIRQFlag == 0 );
				count += CapSense_GetCounter();
				u8CapSenseIRQFlag = 0;
			}
			tpk->tKey[ki].current = count/TP_CALIB_AVG;
			tpk->tKey[ki].lower   = tpk->tKey[ki].current;
			tpk->tKey[ki].grand   = 0;
			ki++;
		}
	}
}

/**
  * @brief  This function is to initiate touch pad controlling for ISD9100 
  * @param  u16Pin is enable pin of capture sense.
  * @return None
  */
void TouchPad_Init( uint16_t u16Pin )
{
	TouchPad_t* tpk = &TouchPad;
	/* reset variable(capture sense interrupt flag, key flag, current pad index) */
	u8CapSenseIRQFlag = 0;
	tpk->index  = 0;
	tpk->status = 0; 
	tpk->enPin  = u16Pin;
	
	/* depend on chip, ISD9100 analog compare gpio (GPB0~GPB7) = 8 */ 
	GPIO_SetMode( PB, (tpk->enPin & 0x00FF), GPIO_MODE_INPUT ); 
	
	/* enable gpio current source */
	CAPSENSE_ENABLE_CURRENT_SOURCE_PIN(	tpk->enPin );
	
	/* select source value for capture sense */
	CapSense_SelectCurrentSourceValue( CAPSENSE_CURCTL0_VALSEL_1000NA );
	
	/* set control configuration(cycle count & low time) */
    CapSense_SetCycleCounts( 4, CAPSENSE_CTRL_LOWTIME_8CYCLES );
	
	NVIC_EnableIRQ(CAPS_IRQn);
	
	/* calibration touch pad value */
	TouchPad_Calib();
	
	/* reset counter & interrupt control for preper scanning */
	TouchPad_Reset();
}

/**
  * @brief  This function is to scan pad pressing or releasing. 
  *         Please locate this function into main loop or cycle interrupt. 
  * @param  None.
  * @return None
  */
void TouchPad_Scan( void )
{
	uint8_t i;
	TouchPad_t* tpk = &TouchPad;
	
	if( u8CapSenseIRQFlag ) {
		for( i=0; i<TP_KEY_NBR_MAX; i++ ) {
			if( tpk->enPin & (0x1 << i) ) {
				tpk->tKey[tpk->index].current = CapSense_GetCounter();
				
				/* Pad Key in Pressing state */
				if( (tpk->tKey[tpk->index].current - tpk->tKey[tpk->index].lower) > THRESHOLD_PRESS[tpk->index]) {
					tpk->tKey[tpk->index].pre_Cnt++;
					tpk->tKey[tpk->index].rel_Cnt = 0;
					if( ((tpk->status & (0x1 << tpk->index)) == 0) && \
						(tpk->tKey[tpk->index].pre_Cnt > TP_GRAND_CNTS) ) {
						tpk->status |= (0x1 << tpk->index);
						tpk->tKey[tpk->index].pre_Cnt = 0;
						tpk->tKey[tpk->index].rel_Cnt = 0;
					}
				}
				/* Pad Key in Releasing state */
				else if ((tpk->tKey[tpk->index].current - tpk->tKey[tpk->index].lower) < THRESHOLD_RELEASE[tpk->index]) {
					tpk->tKey[tpk->index].rel_Cnt++;
					tpk->tKey[tpk->index].pre_Cnt = 0;
					if( ((tpk->status & (0x1 << tpk->index)) != 0 ) && \
						(tpk->tKey[tpk->index].rel_Cnt > TP_GRAND_CNTS) ) {
						tpk->status &= ~(0x1 << tpk->index);
						tpk->tKey[tpk->index].rel_Cnt = 0;
						tpk->tKey[tpk->index].pre_Cnt = 0;
					}
					if( (tpk->tKey[tpk->index].lower - tpk->tKey[tpk->index].current) >  TP_LOW_STEP ) {
						tpk->tKey[tpk->index].grand++;
						if( tpk->tKey[tpk->index].grand>TP_GRAND_CNTS) {
							tpk->tKey[tpk->index].lower -= TP_LOW_STEP;
							tpk->tKey[tpk->index].grand=0;
						}
					}
					else {
						tpk->tKey[tpk->index].grand = 0;
					}
				}
				break;
			}
		}
		u8CapSenseIRQFlag = 0;
		tpk->index++;
		tpk->index %= TP_KEY_NBR;
		ACMP->POSSEL = i << ACMP_POSSEL_POSSEL_Pos;
		
		TouchPad_Reset();
	}
}

uint16_t TouchPad_GetStatus( void )
{
	return( TouchPad.status );
}

void TouchPad_ClrStatus( void )
{
	TouchPad.status &= ~(0x1 << TouchPad.index);
}

