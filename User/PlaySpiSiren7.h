#ifndef PLAY_SPI_SIREN7_H
#define	PLAY_SPI_SIREN7_H

#include <stdbool.h>
#include <stdint.h>
#include <libSPIFlash.h>

#define S7DATA_BASE_ADDR_ON_SPI			0
#define AUDIO_BUFF_SIZE 				160//320
#define AUDIO_BUFF_SIZE_320 			320
#define DPWM_SAMPLERATE  				16000
#define S7BITRATE       				16000
#define S7BANDWIDTH     				7000
#define COMP_BUF_SIZE     				20   	//According to S7BITRATE

extern uint32_t TotG722Size;		// Encoded Siren7 data size
extern uint32_t BuffAddr0;
extern uint32_t BuffAddr1;

extern uint32_t AudioSampCnt;
extern bool bPDMA1CallBackFlg;
extern uint32_t AudioDataAddr;
extern uint32_t BuffEmptyAddr;
extern uint32_t BuffReadyAddr;

extern bool     bPCMPlaying;
extern bool     bBuffEmpty;
extern uint8_t	last2BuffCnt;

extern const SFLASH_CTX g_SPIFLASH;

void PDMA1_Callback( void );
void PlaySpiG722( uint16_t audix );

#endif	/* PLAY_SPI_SIREN7_H */

