#ifndef TIMER_H
#define	TIMER_H

#include <stdbool.h>
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

#define TICK_10_ms                  1
#define TICK_20_ms                  2
#define TICK_50_ms                  5
#define TICK_100_ms                 10
#define TICK_500_ms                 50
#define TICK_1000_ms                100

typedef struct {
	uint32_t trig_10ms:		1;
	uint32_t trig_20ms:		1;
    uint32_t trig_50ms:		1;
    uint32_t trig_100ms:	1;
    uint32_t trig_500ms:	1;
	uint32_t trig_1s:		1;
} TimerFlag_t;

extern TimerFlag_t TimerFlag;

void SysTimerHandler( void );
void SysTimerReset( void );

#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

