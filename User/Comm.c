/*******************************************************************************

   (C) Copyright 2016/7/19, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: comm.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/19
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: 
          Author: Chaoyi Dai
Last Modify Date: 2016/7/19
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


void Comm_Send2Host( void )
{
	uint8_t len = 0;
	uint8_t buff[16];
	sysmgmt_t* mgmt = &SysMgmt;
	
	memset( buff, 0x0, 16 );
	buff[len++] = 0xf0;
	buff[len++] = mgmt->mode;
	buff[len++] = (uint8_t)(mgmt->pm2_5 >> 8);
	buff[len++] = (uint8_t)(mgmt->pm2_5 >> 0);
	buff[len++] = 0xf1;
	UART_Write( UART0, buff, len );
}

typedef enum {
	CS_SOP = 0,
	CS_VOH,
	CS_VOL,
	CS_VREF_H,
	CS_VREF_L,
	CS_CHKSUM,
	CS_EOP,
} comm_state_t;

#define COMM_SOP	0xaa
#define COMM_EOP	0xff
static uint8_t comm_StateStep = CS_SOP;
static uint8_t comm_RxBuff[16];
void Comm_Parsing( uint8_t inbyte )
{
	uint8_t chksum = 0x0;
	sysmgmt_t* mgmt = &SysMgmt;
	switch( comm_StateStep ) {
		case CS_SOP:
			if( inbyte == COMM_SOP ) {
				comm_StateStep++;
			}
			break;
		case CS_VOH:
			comm_RxBuff[0] = inbyte;
			comm_StateStep++;
			break;
		case CS_VOL:
			comm_RxBuff[1] = inbyte;
			comm_StateStep++;
			break;
		case CS_VREF_H:
			comm_RxBuff[2] = inbyte;
			comm_StateStep++;
			break;
		case CS_VREF_L:
			comm_RxBuff[3] = inbyte;
			comm_StateStep++;
			break;
		case CS_CHKSUM:
			chksum = comm_RxBuff[0] + comm_RxBuff[1] + comm_RxBuff[2] + comm_RxBuff[3];
			if( chksum == inbyte ) {
				mgmt->pm25_raw  = (uint16_t)(comm_RxBuff[0] << 8);
				mgmt->pm25_raw += comm_RxBuff[1];
				mgmt->pm25_Vref  = (uint16_t)(comm_RxBuff[2] << 8);
				mgmt->pm25_Vref += comm_RxBuff[3];
				mgmt->status.pm25_rx_update = true;
			}
			comm_StateStep++;
			break;
		case COMM_EOP:
		default:
			memset( (uint8_t*)comm_RxBuff, 0x0, 16 );
			comm_StateStep = CS_SOP;
			break;
	}
}

void Comm_WithPM25Module( void )
{
	uint8_t rxlen = 0;
	uint8_t index = 0;
	uint8_t rxbuf[16];
	
	rxlen = UART_Read( UART0, rxbuf, 16 );
	while( rxlen-- ) {
		Comm_Parsing( rxbuf[index++] );
	}
}

