#ifndef COMM_H
#define	COMM_H

#include <stdbool.h>
#include <stdint.h>

void Comm_Send2Host( void );
void Comm_Parsing( uint8_t inbyte );
void Comm_WithPM25Module( void );

#endif	/* COMM_H */

