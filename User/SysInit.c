/*******************************************************************************

   (C) Copyright 2016/7/19, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: SysInit.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/19
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: V0.01
          Author: Chaoyi Dai
Last Modify Date: 2016/7/19
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


void sys_Init( void )
{
    /* Unlock protected registers */
//	__disable_irq();
    SYS_UnlockReg();
    Clk_Init();
	SysTick_Config( __HIRC/100 );		//tick time: 10ms
	ANA_Init();
	LDO_On();
	SPI_Init();
	Uart_Init();
	tPad_Init();
	
    /* Lock protected registers */
    //SYS_LockReg();
}

void Clk_Init( void )
{
	/* Enable External OSC49M */
    CLK_EnableXtalRC( CLK_PWRCTL_HIRCEN_Msk );
	
    /* Switch HCLK clock source to CLK2X a frequency doubled output of OSC48M */
    CLK_SetHCLK( CLK_CLKSEL0_HCLKSEL_HIRC, CLK_CLKSEL0_HIRCFSEL_48M, CLK_CLKDIV0_HCLK(1) );
	/* Update System Core Clock */
    /* User can use SystemCoreClockUpdate() to calculate SystemCoreClock. */
    SystemCoreClockUpdate();
}

void LDO_On( void )
{
	CLK_EnableLDO( CLK_LDOSEL_3_3V );
}

void SPI_Init( void )
{
	/*---------------------------------------------------------------------------------------------------------*/
    /* Init I/O Multi-function                                                                                 */
    /*---------------------------------------------------------------------------------------------------------*/
    /* SPI0: GPA0=MOSI0, GPA1=SLCKMOSI0, GPA2=SSB0, GPA3=MISO0 */
	SYS->GPA_MFP = (SYS->GPA_MFP & (~SYS_GPA_MFP_PA0MFP_Msk)) | SYS_GPA_MFP_PA0MFP_SPI_MOSI0;
	SYS->GPA_MFP = (SYS->GPA_MFP & (~SYS_GPA_MFP_PA1MFP_Msk)) | SYS_GPA_MFP_PA1MFP_SPI_SCLK;
	SYS->GPA_MFP = (SYS->GPA_MFP & (~SYS_GPA_MFP_PA2MFP_Msk)) | SYS_GPA_MFP_PA2MFP_SPI_SSB0;
	SYS->GPA_MFP = (SYS->GPA_MFP & (~SYS_GPA_MFP_PA3MFP_Msk)) | SYS_GPA_MFP_PA3MFP_SPI_MISO0;	
	/* Reset IP module */
	CLK_EnableModuleClock( SPI0_MODULE );
	SYS_ResetModule( SPI0_RST );
	
    /* Configure SPI0 as a master, MSB first, SPI Mode-0 timing, clock is 1MHz */
	SPI_Open( SPI0, SPI_MASTER, SPI_MODE_0, 100000, 0 );
	
	SPI_SET_MSB_FIRST( SPI0 );
	
	SPI_SET_SUSPEND_CYCLE( SPI0, 8 );
	
	SPI_DISABLE_BYTE_REORDER( SPI0 );
	
	SPI_SET_DATA_WIDTH( SPI0, 32 );
	
	SPI_SET_TX_NUM( SPI0, SPI_TXNUM_ONE );
	
    /* Enable the automatic hardware slave select function */
	SPI_ENABLE_AUTOSS( SPI0, SPI_SS0, SPI_SS_ACTIVE_LOW );
	sflash_set( &g_SPIFLASH );
}

void ANA_Init( void )
{
	CLK_EnableModuleClock( ANA_MODULE );
	SYS_ResetModule( ANA_RST );
}

#define ADC_SAMPLE_RATE  	(16000)
void ADC_Init( void )
{
	uint32_t div;
	/* Enable Analog block power */
	ADC_ENABLE_SIGNALPOWER( ADC,						\
	                        ADC_SIGCTL_ADCMOD_POWER | 	\
						    ADC_SIGCTL_IBGEN_POWER  |	\
	                        ADC_SIGCTL_BUFADC_POWER |	\
	                        ADC_SIGCTL_BUFPGA_POWER |	\
						    ADC_SIGCTL_ZCD_POWER );
	
	/* PGA Setting */
	ADC_MUTEON_PGA( ADC, ADC_SIGCTL_MUTE_PGA );
	ADC_MUTEOFF_PGA( ADC, ADC_SIGCTL_MUTE_IPBOOST );
	ADC_ENABLE_PGA( ADC, ADC_PGACTL_REFSEL_VMID, ADC_PGACTL_BOSST_GAIN_26DB );
	ADC_SetPGAGaindB( 3525 );
	
	/* MIC circuit configuration */
	ADC_ENABLE_VMID( ADC, ADC_VMID_HIRES_DISCONNECT, ADC_VMID_LORES_CONNECT );
	ADC_EnableMICBias( ADC_MICBSEL_90_VCCA );
	ADC_SetAMUX( ADC_MUXCTL_MIC_PATH, ADC_MUXCTL_POSINSEL_NONE, ADC_MUXCTL_NEGINSEL_NONE );
	
	/* ALC Setting */
	CLK->APBCLK0 |= CLK_APBCLK0_BFALCKEN_Msk;
	ADC_ENABLE_ALC( ADC, ADC_ALCCTL_NORMAL_MODE, ADC_ALCCTL_ABS_PEAK, ADC_ALCCTL_FASTDEC_ON );
	ADC_SetALCTargetLevel( -750 );
	
	ADC_SET_ALCDECAYTIME( ADC, 3 );
	ADC_SET_ALCATTACKTIME( ADC, 2 );
	ADC_ENABLE_NOISEGATE( ADC, ADC_ALCCTL_NGPEAK_ABS );
	ADC_SET_NOISEGATE_TH( ADC, ADC_ALCCTL_NGTH7 );
	
	/* Open ADC block */
	/* Set ADC divisor from HCLK */
    CLK_SetModuleClock( ADC_MODULE, MODULE_NoMsk, CLK_CLKDIV0_ADC(1) );
	/* Reset IP */
	CLK_EnableModuleClock( ADC_MODULE );
    SYS_ResetModule( EADC_RST );
	
	ADC_SET_OSRATION( ADC, ADC_OSR_RATION_192 );
	div = CLK_GetHIRCFreq()/ADC_SAMPLE_RATE/192;
	ADC_SET_SDCLKDIV( ADC, div );
	ADC_SET_FIFOINTLEVEL( ADC, 7 );
	
	ADC_MUTEOFF_PGA( ADC, ADC_SIGCTL_MUTE_PGA );
}

void PDMA_Init( void )
{
	/* Reset IP */
	CLK_EnableModuleClock( PDMA_MODULE );
}

void PDMA_Initfor( uint8_t chnnl, uint32_t dst, uint32_t src, uint32_t dir, uint32_t ch4tr, uint32_t buflen )
{
	volatile int16_t i = 10;
	volatile int16_t pos = 0;
	PDMA_T* pdma = (PDMA_T*)((uint32_t)PDMA0_BASE + (0x100 * chnnl));
	PDMA_GCR->GLOCTL |= (0x1 << chnnl) << PDMA_GLOCTL_CHCKEN_Pos;	//PDMA Controller Channel Clock Enable
	
	pdma->DSCT_CTL |= PDMA_DSCT_CTL_SWRST_Msk;   //Writing 1 to this bit will reset the internal state machine and pointers
	pdma->DSCT_CTL |= PDMA_DSCT_CTL_CHEN_Msk;    //Setting this bit to 1 enables PDMA assigned channel operation 
	while(i--);                                  //Need a delay to allow reset
	
	switch( chnnl ) {
		case PDMA_CH_0:		//for mic
			pos = PDMA_SVCSEL_ADCRXSEL_Pos;		//DMA channel is connected to ADC peripheral transmit request.
			break;
		case PDMA_CH_1:		//for dpwm
			pos = PDMA_SVCSEL_DPWMTXSEL_Pos;	//DMA channel is connected to dpwm peripheral transmit request.
			break;
		default:
			break;
	}
	PDMA_GCR->SVCSEL &= ~(0xf << pos);
	PDMA_GCR->SVCSEL |= (chnnl << pos);
	
	pdma->DSCT_ENDSA = (uint32_t)src;			//Set source address
	pdma->DSCT_ENDDA = (uint32_t)dst;			//Set destination address
	//00 = Memory to Memory mode (SRAM-to-SRAM).
    //01 = IP to Memory mode (APB-to-SRAM).
    //10 = Memory to IP mode (SRAM-to-APB).
	dir &= 0x3;
	switch( chnnl ) {
		case PDMA_CH_0:		//for mic
			pdma->DSCT_CTL |= 0x2 << PDMA_DSCT_CTL_SASEL_Pos;    	//Transfer Source address is fixed.
			pdma->DSCT_CTL |= 0x3 << PDMA_DSCT_CTL_DASEL_Pos;    	//Transfer Destination Address is wrapped.
			break;
		case PDMA_CH_1:		//for dpwm
			pdma->DSCT_CTL |= 0x3 << PDMA_DSCT_CTL_SASEL_Pos;    	//Transfer Source address is wrapped.
			pdma->DSCT_CTL |= 0x2 << PDMA_DSCT_CTL_DASEL_Pos;    	//Transfer Destination Address is fixed.
			break;
		default:
			break;
	}
	pdma->DSCT_CTL |= 0x2 << PDMA_DSCT_CTL_TXWIDTH_Pos;  	//One half-word (16 bits) is transferred for every PDMA operation
	pdma->DSCT_CTL |= dir << PDMA_DSCT_CTL_MODESEL_Pos;		//Memory to IP dir (APB-to-SRAM).
	pdma->DSCT_CTL |= 0x5 << PDMA_DSCT_CTL_WAINTSEL_Pos; 	//Wrap Interrupt: Both half and end buffer.
	
	pdma->TXBCCH = buflen;									// Audio array total length, unit: sample.
	
	pdma->INTENCH = 0x1 << PDMA_INTENCH_WAINTEN_Pos;   		//Wraparound Interrupt Enable
	switch( chnnl ) {
		case PDMA_CH_0:		//for mic
			ADC_ENABLE_PDMA( ADC );
			ADC_START_CONV( ADC );
			break;
		case PDMA_CH_1:		//for dpwm
			DPWM_ENABLE_PDMA( DPWM );
			break;
		default:
			break;
	}
	
	NVIC_ClearPendingIRQ( PDMA_IRQn );
	NVIC_EnableIRQ( PDMA_IRQn );
	//pdma->DSCT_CTL |= PDMA_DSCT_CTL_CHEN_Msk;
	//pdma->DSCT_CTL |= PDMA_DSCT_CTL_TXEN_Msk;				//Start PDMA transfer
}

void PDMA_TxEn( uint8_t chnnl, bool enable )
{
	PDMA_T* pdma = (PDMA_T*)((uint32_t)PDMA0_BASE + (0x100 * chnnl));
	if( enable == true ) {
		pdma->DSCT_CTL |= PDMA_DSCT_CTL_CHEN_Msk;
		pdma->DSCT_CTL |= PDMA_DSCT_CTL_TXEN_Msk;				//Start PDMA transfer
	}
	else {
		pdma->DSCT_CTL &= ~PDMA_DSCT_CTL_CHEN_Msk;
		pdma->DSCT_CTL &= ~PDMA_DSCT_CTL_TXEN_Msk;				//Stop PDMA transfer
	}
}

void Uart_Init( void )
{
	/*---------------------------------------------------------------------------------------------------------*/
    /* Init I/O Multi-function                                                                                 */
    /*---------------------------------------------------------------------------------------------------------*/
    /* Set GPG multi-function pins for UART0 RXD and TXD */
	SYS->GPA_MFP = (SYS->GPA_MFP & (~SYS_GPA_MFP_PA8MFP_Msk)) | SYS_GPA_MFP_PA8MFP_UART_TX;
	SYS->GPA_MFP = (SYS->GPA_MFP & (~SYS_GPA_MFP_PA9MFP_Msk)) | SYS_GPA_MFP_PA9MFP_UART_RX;
	/* Reset IP */
	CLK_EnableModuleClock( UART_MODULE );
    SYS_ResetModule( UART0_RST );
	
    /* Configure UART0 and set UART0 Baudrate(2400) */
    UART_Open( UART0, 2400 );
}

void tPad_Init( void )
{
	CLK_EnableModuleClock( ACMP_MODULE );
	SYS_ResetModule( ACMP_RST );
	/* Set GPA & GPB multi-function pins for touch pad */
	SYS->GPB_MFP  = (SYS->GPB_MFP & (~SYS_GPB_MFP_PB7MFP_Msk) ) | SYS_GPB_MFP_PB7MFP_CMP7;
	/* Touch pad initiate configuration */
	TouchPad_Init( CAPSENSE_CURCTL0_CURSRCEN_GPIOB7 );
}

void DPWM_Init( uint32_t SampleRate )
{
	DPWM_Open();
	//DPWMCKSEL |Differential Speaker Driver PWM Clock Source Select
    //0 = OSC48M clock
    //1 = 2x OSC48M clock
	CLK->CLKSEL1 |= BIT4;
	DPWM_SetSampleRate( SampleRate ); 					//Set sample rate
	DPWM_START_PLAY( DPWM );
}

