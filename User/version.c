/*******************************************************************************

   (C) Copyright 2016/7/15, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: version.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/15
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: V0.01
          Author: Chaoyi Dai
Last Modify Date: 2016/7/15
     Discription: Initial version

*******************************************************************************/

#include "includes.h"

static char versionStr[10] = {0};


char* GetVersionStr( void )
{
	int len;

	len = sprintf(versionStr, "%d.%d.%d", VER_MAJOR, VER_MINOR, VER_PATCH);
	versionStr[len] = 0;

	return versionStr;
}

uint32_t GetVersion( void )
{
	uint32_t ver;

	ver = VER_MAJOR * 10000L + VER_MINOR * 100L + VER_PATCH;

	return ver;
}


/* EOF */
