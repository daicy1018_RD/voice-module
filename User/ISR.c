/*******************************************************************************

   (C) Copyright 2016/7/19, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: ISR.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/19
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: 
          Author: Chaoyi Dai
Last Modify Date: 2016/7/19
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


void SysTick_Handler( void )
{
	NVIC_ClearPendingIRQ( SysTick_IRQn );
	SysTimerHandler();
}

#define MAX_CHANNEL_NBR			4
void PDMA_IRQHandler( void )
{
	uint32_t intflg = 0x0;
    uint32_t status = PDMA_GET_INT_STATUS();
    if( status & (0x1 << PDMA_CH_0) ) {				//CH0
		intflg = PDMA_GET_CH_INT_STS( PDMA_CH_0 );
		if( intflg & PDMA_CHIF_TXABTIF_Msk ) {		//0x1 - TXABTIF: PDMA Read/Write Target Abort Interrupt Flag
			PDMA_CLR_CH_INT_FLAG( PDMA_CH_0, PDMA_CHIF_TXABTIF_Msk );
			
		}
        else if( intflg & PDMA_CHIF_TXOKIF_Msk ) {	//0x2 - TXOKIF: Block Transfer Done Interrupt Flag
            PDMA_CLR_CH_INT_FLAG( PDMA_CH_0, PDMA_CHIF_TXOKIF_Msk );
			
		}
		else if( intflg & PDMA_CHIF_WAIF_Msk ) {	//0x0f00 - WAIF: Wrap Around Transfer Byte Count Interrupt Flag
			PDMA_CLR_CH_INT_FLAG( PDMA_CH_0, PDMA_CHIF_WAIF_Msk );
			PDMA0_Callback();
		}
    }
	
    if( status & (0x1 << PDMA_CH_1) ) {				//CH1
		intflg = PDMA_GET_CH_INT_STS( PDMA_CH_1 );
		if( intflg & PDMA_CHIF_TXABTIF_Msk ) {		//0x1 - TXABTIF: PDMA Read/Write Target Abort Interrupt Flag
			PDMA_CLR_CH_INT_FLAG( PDMA_CH_1, PDMA_CHIF_TXABTIF_Msk );
			
		}
        else if( intflg & PDMA_CHIF_TXOKIF_Msk ) {	//0x2 - TXOKIF: Block Transfer Done Interrupt Flag
            PDMA_CLR_CH_INT_FLAG( PDMA_CH_1, PDMA_CHIF_TXOKIF_Msk );
			
		}
		else if( intflg & PDMA_CHIF_WAIF_Msk ) {	//0x0f00 - WAIF: Wrap Around Transfer Byte Count Interrupt Flag
			PDMA_CLR_CH_INT_FLAG( PDMA_CH_1, PDMA_CHIF_WAIF_Msk );
			PDMA1_Callback();
		}
    }
}

