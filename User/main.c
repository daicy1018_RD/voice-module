/*******************************************************************************

   (C) Copyright 2016/7/15, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: main.c
          Author: Chaoyi Dai
Last Modify Date: 2016/7/15
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: 
          Author: Chaoyi Dai
Last Modify Date: 2016/7/15
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


typedef enum STATE_MACHINE {
	STATE_RESET = 0,
	STATE_WORK,
}state_t;

static uint8_t State;
sysmgmt_t SysMgmt;

static void State_Reset( void );
static void State_Work( void );
static void PM25_Calc( void );

void* VR_Init( uint32_t lpBaseModel, uint32_t lpModel );
void VR_Stop( void* hcspotter );
int8_t DoVR_sep( void* hcspotter );
int UnpackBin( uint32_t addr, char* model[], int modlemax, int* fsize );
void Wave_Init( void );
void Wave_StartRecord( void );
void Wave_StopRecord( void );
int16_t Wave_GetSample( int16_t** lppsSample );
void Wave_UnlockSample( int16_t** lppsSample );

void (*const StateFunc[])(void) = {
	State_Reset,
	State_Work,
};

int main( void )
{
	sys_Init();
	State = STATE_RESET;
  	while( true ) {
		StateFunc[State]();
	}
}

void State_Reset( void )
{
	memset( &SysMgmt, 0x0, sizeof(sysmgmt_t) );
	SysTimerReset();
	State = STATE_WORK;
}

#define VR_CMD_TIME_OUT				8
void State_Work( void )
{
	bool   comm_update = false;
	bool   bVOpen = false;
	int8_t nRtn   = -1;
	char*  lppModel[2];	// 0:base, 1:SI
	int    nBinSize    = 0;
	uint16_t timeout   = VR_CMD_TIME_OUT;
	TimerFlag_t* timer = &TimerFlag;
	sysmgmt_t*   mgmt  = &SysMgmt;
	void*    hCSpotter = NULL;
	
	Wave_Init();
	if( UnpackBin((uint32_t)BIN_START_ADDR, lppModel, 2, &nBinSize) < 2 ) {
		//ERR;
	}
	hCSpotter = VR_Init( (uint32_t)lppModel[0], (uint32_t)lppModel[1] );
	while( true ) {
		TouchPad_Scan();
		if( TouchPad_GetStatus() & 0x1 ) {
			TouchPad_ClrStatus();
			if( bVOpen == false ) {
				bVOpen = true;
				VR_Stop( hCSpotter );
				PlaySpiG722( 0 );
				mgmt->mode = 0;
				timeout = VR_CMD_TIME_OUT;
				Wave_Init();
				hCSpotter = VR_Init( (uint32_t)lppModel[0], (uint32_t)lppModel[1] );
			}
		}
		
		nRtn = DoVR_sep( hCSpotter );
		switch( nRtn ) {
			case -1: 	//error
				break;
			case 0:
				VR_Stop( hCSpotter );
				PlaySpiG722( (uint16_t)0 );
				mgmt->mode = 0;
				if( bVOpen == false ) {
					bVOpen = true;
					timeout = VR_CMD_TIME_OUT;
				}
				Wave_Init();
				hCSpotter = VR_Init( (uint32_t)lppModel[0], (uint32_t)lppModel[1] );
				break;
			case 0xB:	//
				VR_Stop( hCSpotter );
				PlaySpiG722( (uint16_t)nRtn );
				mgmt->mode = nRtn;
				bVOpen  = false;
				timeout = 0;
				Wave_Init();
				hCSpotter = VR_Init( (uint32_t)lppModel[0], (uint32_t)lppModel[1] );
				break;
			default:
				VR_Stop( hCSpotter );
				if( bVOpen == true ) {
					if( nRtn > 0 ) {	//audio ix
						PlaySpiG722( (uint16_t)nRtn );
						mgmt->mode = nRtn;
						timeout = VR_CMD_TIME_OUT;
					}
				}
				Wave_Init();
				hCSpotter = VR_Init( (uint32_t)lppModel[0], (uint32_t)lppModel[1] );
				break;
		}
		
		if( timer->trig_50ms == true ) {
            timer->trig_50ms = false;
			comm_update = true;
		}
		if( timer->trig_1s == true ) {
            timer->trig_1s = false;
			if( bVOpen ) {
				if( timeout ) {
					timeout--;
				}
				else {
					bVOpen = false;
					VR_Stop( hCSpotter );
					PlaySpiG722( (uint16_t)0xB );
					mgmt->mode = 0;
					Wave_Init();
					hCSpotter = VR_Init( (uint32_t)lppModel[0], (uint32_t)lppModel[1] );
				}
			}
		}
		Comm_WithPM25Module();
		if( mgmt->status.pm25_rx_update == true ) {
			mgmt->status.pm25_rx_update = false;
			PM25_Calc();
			comm_update = true;
		}
		if( comm_update == true ) {
			comm_update = false;
			Comm_Send2Host();
		}
	}
}

void PM25_Calc( void )
{
	sysmgmt_t* mgmt = &SysMgmt;
	mgmt->pm2_5 = mgmt->pm25_raw * 5.0 * 200 / 1024;	//refer to GP2Y1050AU.pdf
}


//__align(4) int16_t MicBuffer[2][MIC_BUFF_SIZE];
__align(4) int16_t AudioBuff[2][AUDIO_BUFF_SIZE];

bool g_bCallbackFlg = false;
bool g_bRecordStart = false;
bool g_bBlockReady[2] = {0, 0};

__align(4) uint8_t lpMemPool[MEM_SIZE];
__align(4) uint8_t lpState[STATE_SIZE] = { 0 };

void Wave_Init( void )
{
	g_bCallbackFlg   = false;
	g_bBlockReady[0] = false;
	g_bBlockReady[1] = false;
	
	ADC_Init();
	PDMA_Init();
	SYS_UnlockReg();
	PDMA_Initfor( PDMA_CH_0, 		\
	   (uint32_t)&AudioBuff[0][0],	\
	   (uint32_t)&(ADC->DAT), 		\
				  0x1, 				\
				  PDMA_RX_APB, 		\
				 (MIC_BUFF_SIZE * 4) );
	PDMA_TxEn( PDMA_CH_0, true );
}

void Wave_StartRecord( void )
{
	g_bBlockReady[0] = false;
	g_bBlockReady[1] = false;
	g_bRecordStart = true;
}

void Wave_StopRecord( void )
{
	g_bRecordStart = false;
}

int16_t Wave_GetSample( int16_t** lppsSample )
{
	while( g_bRecordStart ) {
		if( g_bBlockReady[0] ) {
			*lppsSample = (int16_t*)&AudioBuff[0][0];
			return( MIC_BUFF_SIZE );
		}
		else if( g_bBlockReady[1] ) {
			*lppsSample = (int16_t*)&AudioBuff[1][0];
			return( MIC_BUFF_SIZE );
		}
	}
	return( 0 );
}

void Wave_UnlockSample( int16_t** lppsSample )
{
	if( *lppsSample == (int16_t*)&AudioBuff[0][0] ) {
		g_bBlockReady[0] = 0;
	}
	if( *lppsSample == (int16_t*)&AudioBuff[1][0] ) {
		g_bBlockReady[1] = 0;
	}
	*lppsSample = NULL;
}

void PDMA0_Callback( void )
{
	if( g_bRecordStart == true ) {
		g_bCallbackFlg = (g_bCallbackFlg == true) ? (false) : (true);
		if( g_bCallbackFlg == true ) {
			g_bBlockReady[0] = 1;
		}
		else {
			g_bBlockReady[1] = 1;
		}
	}
}

int UnpackBin( uint32_t addr, char* model[], int modlemax, int* fsize )
{
	int *lpnBin  = (int*)addr;
	int  nNumBin = lpnBin[0];
	int *lpnBinSize = lpnBin + 1;
	int  i;
	
	model[0] = (char*)(lpnBinSize + nNumBin);
	for( i=1; i<nNumBin; i++ ) {
		if( i >= modlemax ) {
			break;
		}
		model[i] = model[i - 1] + lpnBinSize[i - 1];
	}
	
	*fsize = (uint32_t)(model[i - 1] + lpnBinSize[i - 1]) - addr;
	*fsize = ((*fsize + 0xFFF) >> 12) << 12;	// 4KB alignment for SPI
	
	return( i );
}

void* VR_Init( uint32_t lpBaseModel, uint32_t lpModel )
{
	int32_t  nRet;
	void* hcspotter = NULL;
	CSpotter_GetMemoryUsage_Sep( (uint8_t*)lpBaseModel, (uint8_t*)lpModel, MAX_TIME );
	hcspotter = CSpotter_Init_Sep((uint8_t*)lpBaseModel, (uint8_t*)lpModel, MAX_TIME, lpMemPool, MEM_SIZE, lpState, STATE_SIZE, &nRet);
	if( hcspotter == NULL ) {
		return( NULL );
	}
	
	Wave_StartRecord();
	CSpotter_Reset( hcspotter );
	return( hcspotter );
}

void VR_Stop( void* hcspotter )
{
	Wave_StopRecord();
	CSpotter_Release( hcspotter );
}

int8_t DoVR_sep( void* hcspotter )
{
	int8_t	 nID  = -1;
	int16_t* lpsSample;
	int32_t  nNumSample;
	
	if( (nNumSample = Wave_GetSample(&lpsSample)) > 0 ) {
		if( CSpotter_AddSample(hcspotter, lpsSample, nNumSample) == CSPOTTER_SUCCESS ) {
			nID = CSpotter_GetResult( hcspotter );
		}
		Wave_UnlockSample( &lpsSample );
	}
	return( nID );
}

