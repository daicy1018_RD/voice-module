/*******************************************************************************

   (C) Copyright 2015/10/25, Chaoyi Dai. 	 All rights reserved

----------------- File Information ---------------------------------------------
       File Name: Util.c
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription: 

---------------- Important Notes -----------------------------------------------


----------------- Rivision History ---------------------------------------------
         Version: 
          Author: Chaoyi Dai
Last Modify Date: 2015/10/25
     Discription: Initial version

*******************************************************************************/

#include "includes.h"


static void (*Reset)(void) = (void (*)(void))0x0;

void SoftReset( void )
{
   Reset();
}

uint16_t Utl_CalChkSum16( uint16_t* pu16_Ptr, uint16_t u16_len )
{
   uint16_t u16_i;
   uint16_t u16_sum = 0;

   for( u16_i=0; u16_i<u16_len; u16_i++, pu16_Ptr++ ) {
      u16_sum += *(pu16_Ptr);
   }
	u16_sum = 1 + (~u16_sum);
	
   return( u16_sum );
}

void Utl_MemCopy( uint8_t* pu8_dst, uint8_t* pu8_src, uint16_t u16_len )
{
	uint16_t u16_i;
	
	for( u16_i=0; u16_i<u16_len; u16_i++ ) {
		*pu8_dst++ = *pu8_src++;
	}
}

uint8_t Utl_Compare( uint8_t* pu8_dst, uint8_t* pu8_src, uint16_t u16_len )
{
	uint16_t u16_i;
	
	for( u16_i=0; u16_i<u16_len; u16_i++ ) {
		if( *pu8_dst++ != *pu8_src++ ) {
			return( 1 );
		}
	}
	
	return( 0 );
}

void Utl_MemSet( uint8_t* pu8_dst, uint8_t u8_data, uint16_t u16_len )
{
   uint16_t u16_i;
   
   for( u16_i=0; u16_i<u16_len; u16_i++ ) {
      *pu8_dst++ = u8_data;
   }
}

uint16_t Crc16( uint8_t* byte, uint16_t len )
{
    uint16_t crc = 0xffff;
    while( len-- ) {
        crc = UpdateCrc16(crc, *byte++);
    }
    return( crc );
}

uint16_t UpdateCrc16( uint16_t crc, uint8_t byte )
{
    uint16_t i;
    uint16_t tmp = 0;
	
    crc ^= byte;
    for( i=0; i<8; i++ ) {
        if( crc & 0x0001 ) {
            tmp = crc >> 1;
            tmp &= 0x7fff;
            crc = tmp ^ 0xA001;
        }
        else {
            tmp = crc >> 1;
            crc = tmp & 0x7fff;
        }
    }
    return( crc );
}

uint8_t crc8( uint8_t *data, uint16_t length )
 {
	uint16_t t_crc;
	uint16_t i;
	uint16_t j;
	
	t_crc = 0xFF;
	for( i=0; i<length; i++ ) {
		t_crc ^= data[i];
		for( j=0; j<8; j++ ) {
			if( (t_crc & 0x80) != 0 ) {
				t_crc <<= 1;
				t_crc ^= 0x11D;
			}
			else {
				t_crc <<= 1;
			}
		}
	}
	return( (~t_crc) & 0xFF );
}

void Fifo_Init( __fifo_t* fifo, int32_t* buff, uint16_t bufnbr )
{
	uint8_t i;
	
	fifo->index   = 0;
	fifo->mvBuff  = buff;
	fifo->buffNbr = bufnbr;
	for( i=0; i<bufnbr; i++ ) {
		fifo->mvBuff[i] = 0;
	}
}

int32_t Fifo_Proc( __fifo_t* fifo, int32_t data )
{
	uint16_t i;
	int32_t  temp;
	
	if( fifo->index < fifo->buffNbr ) {
		fifo->mvBuff[fifo->index++] = data;
		temp = 0;
		for( i=0; i<fifo->index; i++ ) {
			temp += fifo->mvBuff[i];
		}
		temp /= fifo->index;
	}
	else {
		for( i=fifo->buffNbr-1; i>0; i-- ) {
			fifo->mvBuff[i] = fifo->mvBuff[i - i];
        }
		fifo->mvBuff[0] = data;
		temp = 0;
		for( i=0; i<fifo->buffNbr; i++ ) {
			temp += fifo->mvBuff[i];
		}
		temp /= fifo->buffNbr;
	}
	return( temp );
}

void Fifo_ClrBuff( __fifo_t* fifo )
{
	uint8_t i;
	
	for( i=0; i<fifo->buffNbr; i++ ) {
		fifo->mvBuff[0] = 0;
	}
}

uint16_t Adc_GetAverageRaw( uint32_t(*pf_func)(uint8_t), uint8_t chnnl )
{
	uint16_t i;
	uint32_t temp;
	uint32_t max1;
	uint32_t max2;
	uint32_t min1;
	uint32_t min2;
	uint32_t sum;
	
	sum = 0L;
	min1 = 0xFFFFFFFF;
	max1 = 0L;
	min2 = 0xFFFFFFFF;
	max2 = 0L;
	
	for( i=0; i<12; i++ ) {
		temp = pf_func( chnnl );
      	if( temp > max1 ) {
         	max2 = max1;
         	max1 = temp;
      	}
      	else {
			if( temp > max2 ) {
				max2 = temp;
			}
		}
		
      	if( temp < min1 ) {
         	min2 = min1;
         	min1 = temp;
      	}
      	else {
			if( temp < min2 ) {
         		min2 = temp;
      		}
		}
      	sum += temp;
   	}
	temp = (sum - max2 - max1 - min2 - min1) >> 3;
	
   	return ( (uint16_t)temp );
}
