/* 
 * File:   Util.h
 * Author: cydai
 *
 * Created on June 24, 2015, 11:30 AM
 */

#ifndef UTIL_H
#define	UTIL_H

#include <stdbool.h>
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

typedef struct {
	uint16_t  index;
	uint16_t  buffNbr;
	int32_t*  mvBuff;
} __fifo_t;
	
void SoftReset( void );
uint16_t Utl_CalChkSum16( uint16_t* pu16_Ptr, uint16_t u16_len );
void Utl_MemCopy( uint8_t* pu8_dst, uint8_t* pu8_src, uint16_t u16_len );
uint8_t Utl_Compare( uint8_t* pu8_dst, uint8_t* pu8_src, uint16_t u16_len );
void Utl_MemSet( uint8_t* pu8_dst, uint8_t u8_data, uint16_t u16_len );
uint16_t Crc16( uint8_t* byte, uint16_t len );
uint16_t UpdateCrc16( uint16_t crc, uint8_t byte );
uint8_t crc8( uint8_t *data, uint16_t length );
void Fifo_Init( __fifo_t* fifo, int32_t* buff, uint16_t bufnbr );
int32_t Fifo_Proc( __fifo_t* fifo, int32_t data );
void Fifo_ClrBuff( __fifo_t* fifo );
uint16_t Adc_GetAverageRaw( uint32_t(*pf_func)(uint8_t), uint8_t chnnl );

#ifdef	__cplusplus
}
#endif

#endif	/* UTIL_H */

