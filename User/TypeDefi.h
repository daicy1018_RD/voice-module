/* 
 * File:   TypeDefi.h
 * Author: cydai
 *
 * Created on June 19, 2015, 9:01 AM
 */

#ifndef TYPEDEFI_H
#define	TYPEDEFI_H

#ifdef	__cplusplus
extern "C" {
#endif


/***********************************************
 * Define NAME as an variable
 * Access of 8/16/32 bit variable:  NAME
 * Access of bit(s):           		NAME_bit.xxx
 ***********************************************/
#define INT8U_BIT(NAME, BIT_STRUCT)     \
union {									\
    uint8_t NAME;                       \
    BIT_STRUCT NAME ## _bit;            \
}
    
#define INT16U_BIT(NAME, BIT_STRUCT)    \
union {									\
    uint16_t NAME;						\
    BIT_STRUCT NAME ## _bit;            \
}

#define INT32U_BIT(NAME, BIT_STRUCT)    \
union {									\
    uint32_t NAME;						\
    BIT_STRUCT NAME ## _bit;            \
}

/***********************************************
 * Define NAME as an variable
 * Access of 8/16/32 bit variable:  NAME
 * Access of enum variable:         NAME_enum
 ***********************************************/
#define INT8U_ENUM(NAME, ENUM_STRUCT)   \
union {									\
    uint8_t NAME;						\
    ENUM_STRUCT NAME ## _enum;          \
}

#define INT16U_ENUM(NAME, ENUM_STRUCT)  \
union {									\
    uint16_t NAME;						\
    ENUM_STRUCT NAME ## _enum;          \
}

#define INT32U_ENUM(NAME, ENUM_STRUCT)  \
union {									\
	uint32_t NAME;						\
	ENUM_STRUCT NAME ## _enum;          \
}

typedef union {
    float f;
    uint8_t b[sizeof(float)];
} float2bytes;

#ifdef	__cplusplus
}
#endif

#endif	/* TYPEDEFI_H */

