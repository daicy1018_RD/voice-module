/*---------------------------------------------------------------------------------------------------------*/
/*                                                                                                         */
/* Copyright(c) 2009 Nuvoton Technology Corp. All rights reserved.                                         */
/*                                                                                                         */
/* Siren7 (G.722) licensed from Polycom Technology                                                         */
/*---------------------------------------------------------------------------------------------------------*/

#include "includes.h"


const SFLASH_CTX g_SPIFLASH;

uint32_t TotG722Size;		// Encoded Siren7 data size
uint32_t BuffAddr0;
uint32_t BuffAddr1;

uint32_t AudioSampCnt;
bool bPDMA1CallBackFlg;
uint32_t AudioDataAddr;
uint32_t BuffEmptyAddr;
uint32_t BuffReadyAddr;

bool      bPCMPlaying;
bool      bBuffEmpty;
uint8_t	  last2BuffCnt;

extern __align(4) int16_t AudioBuff[2][AUDIO_BUFF_SIZE_320];
sSiren7_CODEC_CTL sEnDeCtl;
sSiren7_DEC_CTX   sS7Dec_Ctx;


static void spi_setcs( const struct tagSFLASH_CTX* ctx, int csOn )
{
	// use the SPI controller's CS control
}
const SFLASH_CTX g_SPIFLASH = {
	SPI0_BASE,
	1, // channel 0, auto
	spi_setcs
};

void PDMA1_Callback( void )
{
	if( bPDMA1CallBackFlg == false ) {
		BuffReadyAddr = BuffAddr1;
		BuffEmptyAddr = BuffAddr0;
	}
	else {
		BuffReadyAddr = BuffAddr0;
		BuffEmptyAddr = BuffAddr1;
	}
	bPDMA1CallBackFlg = (bPDMA1CallBackFlg == true) ? (false) : (true);
	
	if( last2BuffCnt <= 2 ) {
		last2BuffCnt++;
		if( last2BuffCnt >= 2 ) {
			bPCMPlaying = false;
		}
	}
	else {
		bBuffEmpty = true;
	}
}

void PlayClose( void )
{
	DPWM_Close();
	PDMA_TxEn( PDMA_CH_1, false );
}

#define NBR_OF_BITS_PER_FRAME		320
#define NBR_OF_BYTE_PER_FRAME		40
void CopySpiSoundData( void )
{
	__align(4) uint8_t buff[NBR_OF_BYTE_PER_FRAME];	//buffer size = dctl.number_of_bits_per_frame/8, 10 for 16Kbps, 5 for 8Kbps
	
	memset( buff, 0x0, NBR_OF_BYTE_PER_FRAME );
	sflash_read( &g_SPIFLASH, (unsigned long)AudioDataAddr, (unsigned long*)buff, NBR_OF_BYTE_PER_FRAME );
	LibS7Decode( &sEnDeCtl, &sS7Dec_Ctx, (int16_t*)buff, (signed short*)BuffEmptyAddr ); 
	
	AudioDataAddr += NBR_OF_BYTE_PER_FRAME;
	AudioSampCnt  += NBR_OF_BITS_PER_FRAME;
	if( AudioSampCnt >= (TotG722Size * 8) ) {	//bits
		last2BuffCnt = 0;
	}
}

void PlayLoop( void )
{
	if( bBuffEmpty == true ) {
		bBuffEmpty = false;
		CopySpiSoundData();
	}
}

void PlaySpiSound( uint32_t dataAddr )
{
	DPWM_Init( 16000 );
	AudioDataAddr = dataAddr;
	bPCMPlaying  = true;
	last2BuffCnt = 0xFF;
	AudioSampCnt = 0;
	bPDMA1CallBackFlg = false;
	
	BuffEmptyAddr = BuffAddr0;
	CopySpiSoundData();
	BuffReadyAddr = BuffAddr0;
	PDMA_Initfor( PDMA_CH_1, 		\
	  (uint32_t)&(DPWM->DATA), 		\
	    (uint32_t)BuffReadyAddr, 	\
				  0x2, 				\
				  PDMA_TX_APB, 		\
				 (AUDIO_BUFF_SIZE_320 * 4) );
	PDMA_TxEn( PDMA_CH_1, true );
	
	BuffEmptyAddr = BuffAddr1;
	bBuffEmpty = true;
}

void PlaySpiG722( uint16_t audix )
{
	uint32_t audPointer;
	uint32_t startAddr;
	uint32_t temp0;
	uint32_t temp1;
	
	PDMA_TxEn( PDMA_CH_0, false );
	
   	BuffAddr0 = (uint32_t)&AudioBuff[0][0];
	BuffAddr1 = (uint32_t)&AudioBuff[1][0];
	
    LibS7Init( &sEnDeCtl, S7BITRATE, S7BANDWIDTH );
    LibS7DeBufReset( sEnDeCtl.frame_size, &sS7Dec_Ctx );
	
    PDMA_Init();
	
	audPointer = S7DATA_BASE_ADDR_ON_SPI + (8 * (audix + 1));
	
	//======================== Important ==========================================================================
	// MSB first and SPI0->CNTRL.BYTE_ENDIAN is 0 in SPI flash library.  Writer have to follow this rule to program.
	sflash_read( &g_SPIFLASH, (audPointer + 0), (unsigned long*)&temp0, 4 );
	sflash_read( &g_SPIFLASH, (audPointer + 8), (unsigned long*)&temp1, 4 );
	
	startAddr   = temp0 + S7DATA_BASE_ADDR_ON_SPI;
	TotG722Size = temp1 - temp0;
	
	if( TotG722Size > NBR_OF_BYTE_PER_FRAME ) {
		PlaySpiSound( startAddr );
		PlayLoop();
	}
	
	while( bPCMPlaying == TRUE ) {
		PlayLoop();
	}
	PlayClose();
}

