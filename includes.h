#ifndef __INCLUDE_H__
#define __INCLUDE_H__

//standard c lib headers
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stdint.h>
#include <ctype.h>
#include <math.h>
#include <stddef.h>

#include "TypeDefi.h"

#include "ISD9100.h"
#include "sys.h"
//#include "timer.h"
#include "gpio.h"
#include "uart.h"
#include "clk.h"
#include "adc.h"
#include "spi.h"
#include "capsense.h"
#include "fmc.h"
#include "dpwm.h"
#include "pdma.h"

#include "libSPIFlash.h"
#include "LibSiren7.h"
#include "CSpotterSDKApi.h"

//compiler headers
#include "Util.h"

//app headers
#include "SysInit.h"
#include "Comm.h"
#include "Tmr.h"
#include "TouchPad.h"
#include "LibSiren7.h"
#include "libSPIFlash.h"
#include "PlaySpiSiren7.h"
#include "ISR.h"
#include "version.h"
#include "main.h"

#endif
